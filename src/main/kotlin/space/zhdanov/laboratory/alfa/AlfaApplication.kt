package space.zhdanov.laboratory.alfa

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication
@EnableCaching
class AlfaApplication

fun main(args: Array<String>) {
    runApplication<AlfaApplication>(*args)
}
