package space.zhdanov.laboratory.alfa.service

import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.CachePut
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import space.zhdanov.laboratory.alfa.entity.domain.Client
import space.zhdanov.laboratory.alfa.exception.NotFoundClientException
import space.zhdanov.laboratory.alfa.repository.ClientRepository
import java.util.*

interface ClientService {
    fun getAll(pageable: Pageable): Page<Client>
    fun save(client: Client): Client
    fun getById(id: UUID): Client
    fun deleteById(id: UUID)
}

@Service
class ClientServiceImpl(
        private val clientRepository: ClientRepository
) : ClientService {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java.enclosingClass)
    }

    override fun getAll(pageable: Pageable): Page<Client> {
        return clientRepository.findAll(pageable)
    }

    @CachePut(value = ["client"], key = "#result.id")
    override fun save(client: Client): Client {
        return clientRepository.save(
                client.takeIf { it.id != null } ?: client.copy(id = UUID.randomUUID())
        )
    }

    @Cacheable(value = ["client"], key = "#id")
    override fun getById(id: UUID): Client {
        logger.info("get client by id: {}", id)
        return clientRepository.findByIdOrNull(id)
                ?: throw NotFoundClientException("Not found client by id: $id")
    }

    @CacheEvict(value = ["client"], key = "#id")
    override fun deleteById(id: UUID) {
        clientRepository.deleteById(id)
    }

}