package space.zhdanov.laboratory.alfa.entity.dto

import space.zhdanov.laboratory.alfa.entity.domain.Client
import java.time.LocalDate
import java.util.*

data class ClientDto(
        val id: UUID,
        val firstName: String,
        val middleName: String?,
        val surname: String,
        val birthday: LocalDate
)

data class ClientPutDto(
        val firstName: String,
        val middleName: String?,
        val surname: String,
        val birthday: LocalDate
)

fun Client.toClientDto() =
        ClientDto(
                id = this.id ?: throw IllegalArgumentException("id can't be null"),
                firstName = this.firstName,
                middleName = this.middleName,
                surname = this.surname,
                birthday = this.birthday
        )

fun ClientDto.toClient() =
        Client(
                id = this.id,
                firstName = this.firstName,
                middleName = this.middleName,
                surname = this.surname,
                birthday = this.birthday
        )

fun ClientPutDto.toClient(id: UUID? = null) =
        Client(
                id = id,
                firstName = this.firstName,
                middleName = this.middleName,
                surname = this.surname,
                birthday = this.birthday
        )