package space.zhdanov.laboratory.alfa.entity.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.time.LocalDate
import java.util.*

@Document
class Client(
        @Id
        val id: UUID?,
        val firstName: String,
        val middleName: String?,
        val surname: String,
        val birthday: LocalDate
) : Serializable {
        fun copy(
                id: UUID? = this.id,
                firstName: String = this.firstName,
                middleName: String? = this.middleName,
                surname: String = this.surname,
                birthday: LocalDate = this.birthday
        ) = Client(
                id = id,
                firstName = firstName,
                middleName = middleName,
                surname = surname,
                birthday = birthday
        )

        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (other !is Client) return false

                if (id != other.id) return false
                if (firstName != other.firstName) return false
                if (middleName != other.middleName) return false
                if (surname != other.surname) return false
                if (birthday != other.birthday) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id?.hashCode() ?: 0
                result = 31 * result + firstName.hashCode()
                result = 31 * result + (middleName?.hashCode() ?: 0)
                result = 31 * result + surname.hashCode()
                result = 31 * result + birthday.hashCode()
                return result
        }

        override fun toString(): String {
                return "Client(id=$id, firstName='$firstName', middleName=$middleName, surname='$surname', birthday=$birthday)"
        }
}