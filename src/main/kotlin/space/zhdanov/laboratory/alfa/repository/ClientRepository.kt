package space.zhdanov.laboratory.alfa.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import space.zhdanov.laboratory.alfa.entity.domain.Client
import java.util.*

@Repository
interface ClientRepository : MongoRepository<Client, UUID>