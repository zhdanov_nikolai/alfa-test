package space.zhdanov.laboratory.alfa.controller

import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import space.zhdanov.laboratory.alfa.entity.dto.ClientDto
import space.zhdanov.laboratory.alfa.entity.dto.ClientPutDto
import space.zhdanov.laboratory.alfa.entity.dto.toClient
import space.zhdanov.laboratory.alfa.entity.dto.toClientDto
import space.zhdanov.laboratory.alfa.service.ClientService
import java.util.*

@RestController
@RequestMapping("clients")
class ClientController(
        private val clientService: ClientService
) {

    @GetMapping
    fun getAll(pageable: Pageable): ResponseEntity<List<ClientDto>> {
        val clients = clientService.getAll(pageable)
        return ResponseEntity.ok()
                .header("x-total-count", clients.totalElements.toString())
                .body(clients.content.map { it.toClientDto() })
    }

    @GetMapping("{id}")
    fun getById(@PathVariable id: UUID): ResponseEntity<ClientDto> {
        return ResponseEntity.ok(clientService.getById(id).toClientDto())
    }

    @PostMapping
    fun createClient(@RequestBody body: ClientPutDto): ResponseEntity<ClientDto> {
        return ResponseEntity.ok(
                clientService.save(body.toClient()).toClientDto()
        )
    }

    @PutMapping("{id}")
    fun updateClient(@PathVariable id: UUID,
                     @RequestBody body: ClientPutDto): ResponseEntity<ClientDto> {
        return ResponseEntity.ok(
                clientService.save(body.toClient(id)).toClientDto()
        )
    }

    @DeleteMapping("{id}")
    fun deleteById(@PathVariable id: UUID): ResponseEntity<Unit> {
        clientService.deleteById(id)
        return ResponseEntity.noContent().build()
    }
}